#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGroupBox>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QDebug>

//------------------------------------------------------------------------------- ctor dtor

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    // set pointer to NULL
    video = NULL;

    initGui();
}

MainWindow::~MainWindow()
{
    qDebug() << "MainWindow dtor";
    closeVideo();
    delete ui;
}

void MainWindow::initGui()
{
    ui->setupUi(this);

    gl = new OpenCV_GLWidget();
    gl->setParent(this->centralWidget());

    connect(ui->camera, SIGNAL(toggled(bool)),
            this, SLOT(cameraToggled(bool)));
    connect(ui->video, SIGNAL(toggled(bool)),
            this, SLOT(videoToggled(bool)));
    ui->camera->toggle();

    ui->FPS_slider->setMaximum(4000);
    ui->FPS_slider->setMinimum(15);
    connect(ui->FPS_slider, SIGNAL(valueChanged(int)),
            this, SLOT(changeFrameRate(int)));
}

void MainWindow::closeVideo()
{
    if(video != NULL) {
        video->terminate();
        video->deleteLater();
        sleep(1);
    }
    video = NULL;
}

//------------------------------------------------------------------------------- GUI SLOTS

void MainWindow::changeFrameRate(int n)
{
    if(video != NULL) {
        video->setFrameRate(n);
        ui->FPS_label->setText( QString("FPS: ")+QString::number(1000/n) );
    } else {
        qDebug() << "try to set framerate but video source is NULL";
    }
}

void MainWindow::cameraToggled(bool n)
{
    if(!n) return;
    closeVideo();

    video = new VideoWorker(0);
    connect(video, SIGNAL(sendImage(QImage)), gl, SLOT(renderImage(QImage)));
    video->start();

    ui->FPS_slider->setValue(50);
    changeFrameRate(50);
}

void MainWindow::videoToggled(bool n)
{
    if(!n) return;
    closeVideo();

    video = new VideoWorker("/Users/alberto/Movies/station.mov");
    connect(video, SIGNAL(sendImage(QImage)), gl, SLOT(renderImage(QImage)));
    connect(video, SIGNAL(finished()), this, SLOT(restartVideo()));
    video->start();

    ui->FPS_slider->setValue(50);
    changeFrameRate(50);
}

void MainWindow::restartVideo()
{
    videoToggled(true);
}
