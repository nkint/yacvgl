#-------------------------------------------------
#
# Project created by QtCreator 2013-07-29T01:06:38
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = YACVGL
TEMPLATE = app

#------------------------------------------------- opencv
macx {
    QMAKE_CFLAGS_X86_64 += -mmacosx-version-min=10.7
    QMAKE_CXXFLAGS_X86_64 = $$QMAKE_CFLAGS_X86_64

        message("* Using settings for Mac OS X.")
    INCLUDEPATH += /usr/local/include/opencv

    LIBS += -L/usr/local/lib/ \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_imgproc -framework GLUT -framework OpenGL -framework Cocoa
}

SOURCES += main.cpp\
        mainwindow.cpp \
    opencv_glwidget.cpp \
    videothread.cpp

HEADERS  += mainwindow.h \
    opencv_glwidget.h \
    videothread.h

FORMS    += mainwindow.ui

RESOURCES +=
