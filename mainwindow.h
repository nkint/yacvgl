#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "videothread.h"
#include "opencv_glwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void initGui();
private:
    Ui::MainWindow *ui;
    OpenCV_GLWidget *gl;

    VideoWorker *video;
    void closeVideo();

private slots:
    void changeFrameRate(int n);
    void cameraToggled(bool n);
    void videoToggled(bool n);
    void restartVideo();
};

#endif // MAINWINDOW_H
