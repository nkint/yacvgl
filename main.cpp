#include "mainwindow.h"

#include <iostream>
#include <QApplication>
#include <QFile>
#include <QFont>
#include <QFontDatabase>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
