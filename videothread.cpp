#include "videothread.h"thread

#include <QDebug>

//------------------------------------------------------------------------------- ctor dtor

VideoWorker::VideoWorker(std::string source, QObject *parent) :
    QThread(parent),
    emptyFrameCounter(0),
    m_FrameRate(20)
{
    video = cv::VideoCapture(source);
    qDebug() << "VideoThread > ctor.";
}

VideoWorker::VideoWorker(int source, QObject *parent) :
    QThread(parent),
    emptyFrameCounter(0),
    m_FrameRate(20)
{
    video = cv::VideoCapture(source);
    qDebug() << "VideoThread > ctor.";
}

VideoWorker::~VideoWorker()
{
    qDebug() << "VideoThread > dtor";
    endCapture();
    wait();
}

//------------------------------------------------------------------------------- run

void VideoWorker::run()
{
    m_AbortCapture = false;
    qDebug() << "VideoThread::run..";

    while(true)
    {
        m_AbortCaptureLock.lock();
        if (m_AbortCapture) {
            qDebug() << "VideoThread::run > abort capture..";
            break;
        }
        m_AbortCaptureLock.unlock();

        cv::Mat cvFrame;
        video >> cvFrame;
        if(cvFrame.empty())  {
            emptyFrameCounter++;
            if( emptyFrameCounter>ABORT_POLICY_LIMIT ) break;
            continue;
        }

        // convert the Mat to a QImage
        QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
        qtFrame = qtFrame.rgbSwapped();

        // queue the image to the gui
        emit sendImage(qtFrame);

        m_FrameRateLock.lock();
        msleep(m_FrameRate);
        m_FrameRateLock.unlock();
    }
    emit finished();
}

bool VideoWorker::endCapture()
{
    qDebug() << "VideoThread::endCapture()";

    QMutexLocker abortLocker(&m_AbortCaptureLock);
    if(video.isOpened()) {
        video.release();
        qDebug() << "Camera successfully disconnected.";
        return true;
    } else {
        qDebug() << "video object was not opened.. nothing to close";
        return false;
    }
    m_AbortCapture = true;
}

//------------------------------------------------------------------------------- getter && setter

void VideoWorker::setFrameRate(int frameRate)
{
    QMutexLocker fpsLocker(&m_FrameRateLock);
    this->m_FrameRate = frameRate;
}

int VideoWorker::getFrameRate()
{
    QMutexLocker fpsLocker(&m_FrameRateLock);
    qDebug() << "VideoWorker::getFrameRate()" << m_FrameRate;
    return m_FrameRate;
}
