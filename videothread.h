#ifndef VIDEOTHREAD_H
#define VIDEOTHREAD_H

#include <string>
#include <QMutex>
#include <QImage>
#include <QThread>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/video.hpp>

// TODO: move to Enum / Setting Object
#define ABORT_POLICY_LIMIT 7

class VideoWorker : public QThread
{
    Q_OBJECT
public:
    explicit VideoWorker(std::string source, QObject *parent = 0);
    explicit VideoWorker(int source, QObject *parent = 0);
    ~VideoWorker();

    void setFrameRate(int frameRate);
    int getFrameRate();

protected:
    void run();

private:
    cv::VideoCapture video;
    int emptyFrameCounter;

    QMutex m_AbortCaptureLock;
    bool m_AbortCapture;

    QMutex m_FrameRateLock;
    int m_FrameRate;

signals:
    void sendImage(QImage);
    void finished();

public slots:
    bool endCapture();
};

#endif // VIDEOTHREAD_H
